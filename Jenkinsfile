pipeline {
    //agent master-label
    agent none
/*
    agent {
        docker {
            image 'docker'
            args '-v /var/run/docker.sock:/var/run/docker.sock --entrypoint=""'
        }
    }
*/
    environment {
        appVersion = "${getTimestamp()}"
    }

    stages {
        stage('Build') {
            agent {
                docker {
                    image 'docker'
                    args '-v /var/run/docker.sock:/var/run/docker.sock --entrypoint=""'
                }
            }
            steps {
                sh(returnStdout: true, script: '''#!/bin/sh
                if ! `docker image inspect gdromard/springboot-blueprint &> /dev/null`; then
                    echo 'Here we are testing that docker configuration is correct and that Jenkins agent as docker are correctly running'
                    echo 'Then we are testing that we are able to build a docker image (inside a dockerised jenkins agent) => means that docker in docker is working well'
                    docker build -t gdromard/springboot-blueprint .

                    echo 'Then we are testing that we are able to push docker image to nexus docker registry'
                    export PROJECT_VERSION=$(./gradlew properties -q | grep "version:" | awk '{print $2}')
                    docker tag gdromard/springboot-blueprint ubuntu.local/gdromard/springboot-blueprint:$PROJECT_VERSION
                    docker login -u admin -p admin123 ubuntu.local
                    docker tag gdromard/springboot-blueprint ubuntu.local/gdromard/springboot-blueprint:$PROJECT_VERSION
                    docker push ubuntu.local/gdromard/springboot-blueprint:$PROJECT_VERSION
                    docker logout
                fi
                '''.stripIndent())
            }
        }

        stage('Test Tools') {
            agent {
                label 'master'
            }
            tools {
                jdk 'jdk-default'
                gradle 'gradle-default'
                maven 'maven-default'
                git 'git-default'
                ant 'ant-default'
            }
            steps {
                sh '''
                    echo "PATH = ${PATH}"
                    echo "M2_HOME = ${M2_HOME}"
                    echo "JAVA_HOME = ${JAVA_HOME}"
                    echo "GRADLE_HOME = ${GRADLE_HOME}"
                    echo "ANT_HOME = ${ANT_HOME}"
                    echo "GIT_HOME = ${GIT_HOME}"
                    echo 'Here we are testing that MAVEN default install is working well'
                    mvn --version
                    echo 'Here we are testing that ANT default install is working well'
                    ant -version
                    echo 'Here we are testing that GRADLE default install is working well'
                    docker -v
                    echo 'Here we are testing that GRADLE default install is working well'
                    ./gradlew --version
                    echo 'Here we are testing that JAVA default install is working well'
                    ${JAVA_HOME}/bin/java --version
                '''
            }
        }

        stage('Deploy') {
            agent {
                docker {
                    image 'docker'
                    args '-v /var/run/docker.sock:/var/run/docker.sock --entrypoint=""'
                }
            }

            steps {
                sh '''
                    echo 'Here we are testing that application is correctly responding and test are OK => means that docker is well configured !'
                    docker run --rm -d --name test-run -p 8080:8080 gdromard/springboot-blueprint
                '''
            }
        }

        stage('Test TI') {
            agent {
                docker {
                    image 'gradle:6.9-jdk11'
                    args '-v /var/run/docker.sock:/var/run/docker.sock --entrypoint=""'
                }
            }

            steps {
                sh '''
                    echo 'Here we are testing that application is correctly responding and test are OK => means that docker image has been successfully started !'
                    ./gradlew build -x test --no-daemon
                '''
            }
        }

        stage('Stop') {
            agent {
                label 'master'
            }

            steps {
                sh '''
                    docker stop test-run
                '''
            }
        }
    }
}

def getTimestamp() {
    def now = new java.util.Date()
    return now.format("YYYYMMdd-HHmmss")
}
