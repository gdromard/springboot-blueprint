
package net.dromard.blueprint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@SpringBootApplication
@RestController
public class Application {

    @RequestMapping("/")
    public String home() {
        return "Hello Docker World";
    }

    @RequestMapping("/version")
    public String version() throws IOException {
        return getVersion();
    }

    @RequestMapping("/details")
    public Map<String, String> details() throws IOException {
        return getAppDetails();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    private Attributes getManifestAttributes() throws IOException {
        URL url = this.getClass().getClassLoader().getResource("META-INF/MANIFEST.MF");
        System.out.println("[DEBUG] MANIFEST: " + url);
        Manifest manifest = new Manifest(url.openStream());
        if (manifest != null) return manifest.getMainAttributes();
        return null;
    }

    private Map<String, String> getAppDetails() throws IOException {
        Attributes attr = getManifestAttributes();
        Map<String, String> map = new HashMap<>();
        if (attr == null) return map;
        map.put("createdBy", attr.getValue("Created-By"));
        map.put("builtBy", attr.getValue("Built-By"));
        map.put("buildDate", attr.getValue("Build-Date"));
        map.put("builtVersion", attr.getValue("Built-Version"));
        map.put("projectName", attr.getValue("Project-Name"));
        return map;
    }

    private String getVersion() throws IOException {
        Attributes attr = getManifestAttributes();
        if (attr == null) return null;
        return attr.getValue("Built-Version");
        //return ApplicationVersion.getVersion();
    }
}
