package net.dromard.blueprint;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class SpringBootApplicationTest {
    @Autowired
    Application application;

    @Test
    void testSpringBootHome() {
        assertEquals("Hello Docker World", application.home(), "Version from REST API should map the same version of the MANIFEST.MF");
    }

    @Test
    void testSpringBootVersion() throws IOException {
        assertEquals(getVersion(), application.version(), "Version from REST API should map the same version of the MANIFEST.MF");
    }

    private Attributes getManifestAttributes() throws IOException {
        URL url = this.getClass().getClassLoader().getResource("META-INF/MANIFEST.MF");
        System.out.println("[DEBUG] MANIFEST: " + url);
        Manifest manifest = new Manifest(url.openStream());
        if (manifest != null) return manifest.getMainAttributes();
        return null;
    }

    private String getVersion() throws IOException {
        Attributes attr = getManifestAttributes();
        if (attr == null) return null;
        return attr.getValue("Built-Version");
    }
}

