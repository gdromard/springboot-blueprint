package net.dromard.blueprint;

import net.dromard.blueprint.Application;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class HttpClientApplicationIT {
    private static final String APPLICATION_ROOT_URL = "http://localhost:8080/";

    @BeforeAll
    static void setup() {
        System.out.println("@BeforeAll - executes once before all test methods in this class");
    }

    @BeforeEach
    void init() {
        System.out.println("@BeforeEach - executes before each test method in this class");
    }

    @Test
    void testHelloWorld() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(APPLICATION_ROOT_URL)).build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

        assertEquals(200, response.statusCode(), "Version REST API should return 200 HTTP CODE STATUS");
        assertEquals("Hello Docker World", response.body(), "Version from REST API should map the same version of the MANIFEST.MF");
    }

    @Test
    void testVersion() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create(APPLICATION_ROOT_URL + "version"))
            .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

        assertEquals(200, response.statusCode(), "Version REST API should return 200 HTTP CODE STATUS");
        String version = getVersion();
        if (version == null) version = "0.0.1-SNAPSHOT";
        assertEquals(version, response.body(), "Version from REST API should map the same version of the MANIFEST.MF");
    }

    private Attributes getManifestAttributes() throws IOException {
        URL url = this.getClass().getClassLoader().getResource("META-INF/MANIFEST.MF");
        System.out.println("[DEBUG] MANIFEST: " + url);
        Manifest manifest = new Manifest(url.openStream());
        if (manifest != null) return manifest.getMainAttributes();
        return null;
    }

    private String getVersion() throws IOException {
        Attributes attr = getManifestAttributes();
        if (attr == null) return null;
        return attr.getValue("Built-Version");
    }
}

