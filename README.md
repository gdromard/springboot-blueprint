# README

## Getting Start

### Clone repository:

    git clone https://bitbucket.org/gdromard/springboot-blueprint.git

### Build docker image

    docker build -t gdromard/springboot-blueprint .

### Run docker image
    
    docker run --name="springboot-blueprint" -it -p 8080:8080 gdromard/springboot-blueprint
